<?php
defined('TYPO3_MODE') || die('Access denied.');

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/**
 * register cache for extension
 */
if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kitt3nservices_cache'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kitt3nservices_cache'] =
        array();
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kitt3nservices_cache']['frontend'] =
        'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kitt3nservices_cache']['backend'] =
        'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kitt3nservices_cache']['options']['compression'] =
        1;
}

/*
 * add hooks to clear cache
 */
// The Backend-MenuItem in ClearCache-Pulldown
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['additionalBackendItems']['cacheActions'][] = KITT3N\Kitt3nServices\Hooks\ClearCacheHook::class;

// The AjaxCall to clear the cache
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][] = 'KITT3N\\Kitt3nServices\\Hooks\\ClearCacheHook->clear';

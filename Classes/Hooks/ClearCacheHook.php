<?php

namespace KITT3N\Kitt3nServices\Hooks;

/***
 *
 * This file is part of the "KITT3N | kitt3n services" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use KITT3N\Kitt3nServices\Cache\CacheManager as CustomCacheManager;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\DataHandling\DataHandler;

class ClearCacheHook implements \TYPO3\CMS\Backend\Toolbar\ClearCacheActionsHookInterface
{

    /**
     * Add an entry to the CacheMenuItems array
     *
     * @param array $cacheActions Array of CacheMenuItems
     * @param array $optionValues Array of AccessConfigurations-identifiers (typically  used by userTS with options.clearCache.identifier)
     *
     * @return
     */
    public function manipulateCacheActions(&$cacheActions, &$optionValues)
    {

        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

        $cacheActions[] = array(
            'id' => 'kitt3n_services',
            'title' => "LLL:EXT:kitt3n_services/Resources/Private/Language/translation.xlf:tx_kitt3n_services.cache.clear.title",
            'description' => "LLL:EXT:kitt3n_services/Resources/Private/Language/translation.xlf:tx_kitt3n_services.cache.clear.description",
            'href' => (string)$uriBuilder->buildUriFromRoute('tce_db', ['cacheCmd' => 'flush_tags:kitt3n_services']),
            'iconIdentifier' => 'kitt3n_cat_svg_white'
        );

        $cacheActions[] = array(
            'id' => 'kitt3n_services',
            'title' => "LLL:EXT:kitt3n_services/Resources/Private/Language/translation.xlf:tx_kitt3n_services.cache.all_but_not_kitt3nservices_cache.title",
            'description' => "LLL:EXT:kitt3n_services/Resources/Private/Language/translation.xlf:tx_kitt3n_services.cache.all_but_not_kitt3nservices_cache.description",
            'href' => (string)$uriBuilder->buildUriFromRoute('tce_db',
                ['cacheCmd' => 'flush_all_but_keep_cacheconfigurations:kitt3nservices_cache']),
            'iconIdentifier' => 'kitt3n_cat_svg_gray'
        );

    }

    /**
     * This method is called by the CacheMenuItem in the Backend
     *
     * @param \array                                   $_params
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler
     */
    public static function clear($_params, $dataHandler)
    {

        /*
         * @example flush_tags:kitten_services,any_other_tag
         */
        if (strpos($_params['cacheCmd'], 'flush_tags:') !== false) {

            $aFlushTags = explode(',', explode(':', $_params['cacheCmd'])[1]);

            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            /** @var CacheManager $oCacheManager */
            $oCacheManager = $objectManager->get(CacheManager::class);

            foreach ($aFlushTags as $sTag) {
                $oCacheManager->flushCachesByTag($sTag);
            }

        }

        /*
         * Keeps 'kitt3nservices_cache' cache in any case!
         *
         * @example flush_all_but_keep_cacheconfigurations:kitt3nservices_cache,any_other_cacheconfiguration
         */
        if (strpos($_params['cacheCmd'], 'flush_all_but_keep_cacheconfigurations:') !== false) {

            $aKeepConfigurations = explode(',', explode(':', $_params['cacheCmd'])[1]);

            /*
             * 'kitt3nservices_cache' should not be flushed
             * Otherwise write another function in your extension
             */
            if (! in_array('kitt3nservices_cache', $aKeepConfigurations)) {
                $aKeepConfigurations[] = 'kitt3nservices_cache';
            }

            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            /** @var CustomCacheManager $oCustomCacheManager */
            $oCustomCacheManager = $objectManager->get(CustomCacheManager::class);

            $aCacheConfigurations = $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'];

            foreach ($aCacheConfigurations as $sCacheConfigurationKey => $aCacheConfiguration) {
                if (in_array($sCacheConfigurationKey, $aKeepConfigurations)) {
                    unset($aCacheConfigurations[$sCacheConfigurationKey]);
                }
            }

            $oCustomCacheManager->overrideCacheConfigurations($aCacheConfigurations);
            $oCustomCacheManager->flushCaches();

        }

    }

}
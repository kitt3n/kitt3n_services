<?php
namespace KITT3N\Kitt3nServices\Service;

/***
 *
 * This file is part of the "KITT3N | kitt3n services" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

class CacheService
{
    /**
     * @param string|array $vCacheIdentifier
     * @return mixed
     */
    public function getCache(
        $vCacheIdentifier
    ) {

        if (is_array($vCacheIdentifier)) {
            $sCacheIdentifier = implode('_', $this->sanitizeCacheIdentifier($vCacheIdentifier));
        } elseif (is_string($vCacheIdentifier)) {
            $sCacheIdentifier = $this->sanitizeCacheIdentifier($vCacheIdentifier);
        } else {
            throw new \InvalidArgumentException(
                '$vCacheIdentifier was not an array or string as expected.',
                84948390302
            );
        }

        $sContent = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Core\\Cache\\CacheManager')
            ->getCache('kitt3nservices_cache')
            ->get($sCacheIdentifier);
        return $sContent;

    }

    /**
     * @param string|array $vCacheIdentifier
     * @param string $sContent
     * @param int $iCachePeriod
     * @param array $aTags
     */
    public function setCache(
        $vCacheIdentifier,
        string $sContent,
        int $iCachePeriod,
        array $aCacheTags = []
    ) {

        if (is_array($vCacheIdentifier)) {
            $sCacheIdentifier = implode('_', $this->sanitizeCacheIdentifier($vCacheIdentifier));
        } elseif (is_string($vCacheIdentifier)) {
            $sCacheIdentifier = $this->sanitizeCacheIdentifier($vCacheIdentifier);
        } else {
            throw new \InvalidArgumentException(
                '$vCacheIdentifier was not an array or string as expected.',
                84948390303
            );
        }

        $aCacheTags[] = 'kitt3n_services';
        $aTags = $aCacheTags;

        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Core\\Cache\\CacheManager')
            ->getCache('kitt3nservices_cache')
            ->set(
                $sCacheIdentifier,
                $sContent,
                $aTags,
                $iCachePeriod
            );

    }

    /**
     * @param string|array $vCacheIdentifier
     */
    public function removeCache(
        $vCacheIdentifier
    ) {

        if (is_array($vCacheIdentifier)) {
            $sCacheIdentifier = implode('_', $this->sanitizeCacheIdentifier($vCacheIdentifier));
        } elseif (is_string($vCacheIdentifier)) {
            $sCacheIdentifier = $this->sanitizeCacheIdentifier($vCacheIdentifier);
        } else {
            throw new \InvalidArgumentException(
                '$vCacheIdentifier was not an array or string as expected.',
                84948390301
            );
        }

        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Core\\Cache\\CacheManager')
            ->getCache('kitt3nservices_cache')
            ->remove($sCacheIdentifier);

    }

    /**
     * @param string|array $vCacheIdentifier
     */
    private function sanitizeCacheIdentifier (
        $vCacheIdentifier
    ) {

        if (is_array($vCacheIdentifier)) {
            $vCacheIdentifier = array_map(function($value){
                return preg_replace('/[^a-zA-Z\\d\\_]+/u', '', $value);
            }, $vCacheIdentifier);
        } elseif (is_string($vCacheIdentifier)) {
            $vCacheIdentifier = preg_replace('/[^a-zA-Z\\d\\_]+/u', '', $vCacheIdentifier);
        } else {
            throw new \InvalidArgumentException(
                '$vCacheIdentifier was not an array or string as expected.',
                84948390301
            );
        }
        return $vCacheIdentifier;
    }
}